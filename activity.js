let trainer = {
	name: "Ash",
	age: 13,
	address:{
		city: "Pallet Town",
		country: "Japan"
	},
	friends: [],
	pokemons: [],

	catch : function(pokemon){
		if (this.pokemons.length < 6){
			console.log(`Gotcha, ${pokemon}!`);
			this.pokemons.push(pokemon);
		} else{
			console.log("A trainer should only have 6 pokemons to carry.")
		}
	},

	release : function(){
		if (this.pokemons.length > 0){
			this.pokemons.pop();
		}else{
			console.log("You have no more pokemons! Catch one first.")
		}
	} 
}


function Pokemon(name, type, level){
	this.name = name;
	this.type = type;
	this.level = level;
	this.hp = 3 * level ;
	this.atk = 2.5 * level;
	this.def = 2.5 * level;
	this.isFainted = false;

	this.tackle = function(pokemon){
		if (pokemon.hp > 0 && pokemon.isFainted == false){
			pokemon.hp -= this.atk;
			console.log(`${this.name} tackled ${pokemon.name}!`);
			if(pokemon.hp < 0){
				pokemon.faint();
			}
		} else{
			console.log(`${pokemon.name} has already fainted`);
		}
		
	};

	this.faint = function(){
		console.log(`${this.name} has fainted.`);
		alert(`${this.name} has fainted.`);
		this.isFainted = true;
	};
}

let pokemon1 = new Pokemon("Zubat", "Poison/Flying", 20);
let pokemon2 = new Pokemon("Pikachu", "Electric", 25);


